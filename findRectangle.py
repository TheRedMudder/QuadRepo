from builtins import print
import cv2
# Get Image and Filter it
img_loc = 'source/yolo/sample_img/shapes.png'  # Image Location
img = cv2.imread(img_loc, cv2.IMREAD_COLOR)
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # Convert To Gray
img_blur = cv2.GaussianBlur(img_gray, (5, 5), 0) # Blur
img_tresh = cv2.threshold(img_blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
# Detect Contour
_, contours, _ = cv2.findContours(img_tresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img,contours,-1,(255,0,0),3) # Draw All Contours in Blue
for contour in contours:
    contour_moments=cv2.moments(contour); # Find Properties
    contour_X = int((contour_moments["m10"] / contour_moments["m00"]) )
    contour_Y = int((contour_moments["m01"] / contour_moments["m00"]) )
    # Find Rectangle
    verticies = len(cv2.approxPolyDP(contour, 0.04 * cv2.arcLength(contour, True), True))
    if verticies==4:
        cv2.putText(img, 'Rect', (contour_X, contour_Y), cv2.FONT_HERSHEY_SIMPLEX,0.5, (0, 0, 255), 1)
        cv2.drawContours(img,[contour],-1,(0,0,255),3)
# Display Results
cv2.imshow("Gray", img_gray)
cv2.imshow("Mask", img_tresh)
cv2.imshow("Img", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
