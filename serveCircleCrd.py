import socket
import time
import multiprocessing
import cv2
import numpy as np
import math
UDP_Port=5843 # The port to send the coordinates from (Computer)
AFD_AddrPort=('192.168.1.3',UDP_Port) # The phones IP and Port to send the Coordinates to. (Android)
def CoordinatesSenderSocket(unitX,unitY,extraD):
    s =socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # Initialize Socket
    ip=socket.gethostbyname(socket.gethostname()) # Get Local IP, Make Sure VirtualBox or Extra IPv4 are disabled
    ip = "192.168.1.2"
    print("This systems local IP is: "+ip)
    s.bind((ip,5841)) # Bind IP and Port
    while True:
        sendCooridnates = "(" + str(unitX.value) + ", " + str(unitY.value) + ", " + str(extraD.value) + ")"  # Format Coordinates as (x,y) Ex: (.2331,-.123)
        data=sendCooridnates.encode('utf-8')# Encode before sending
        print("Sending: "+sendCooridnates)
        time.sleep(.1)
        s.sendto(data,AFD_AddrPort)
    c.close()

if __name__=='__main__':
    unitX=multiprocessing.Value('d',0) # Create shared memory double
    unitY=multiprocessing.Value('d',0) # Create shared memory double
    extraD=multiprocessing.Value('d',0) # Create shared memory double
    multiprocessing.Process(target=CoordinatesSenderSocket, args=(unitX,unitY,extraD)).start() #Start CoordinatesSenderSocket on a separate process
    # Create Trackbar
    def emptyFunc(dontcare):
        pass
    CoordinateTrackBar = "CoordinateTrackBar"
    cv2.namedWindow(CoordinateTrackBar)

    minangle=90
    maxangle=270
    angle=minangle
    uX = 0
    uY = 0
    increment=1
    cv2.createTrackbar('ExtraData', CoordinateTrackBar, 0, 3, emptyFunc)
    cv2.createTrackbar('MinAngle', CoordinateTrackBar, minangle, 360, emptyFunc)
    cv2.createTrackbar('MaxAngle', CoordinateTrackBar, maxangle, 360, emptyFunc)
    cv2.createTrackbar('Increment', CoordinateTrackBar, increment, 10, emptyFunc)
    while True:

        frame = np.zeros((256, 256, 3), np.uint8)  # Create Blank Image
        height, width, _ = frame.shape
        minangle = cv2.getTrackbarPos('MinAngle', CoordinateTrackBar)
        maxangle = cv2.getTrackbarPos('MaxAngle', CoordinateTrackBar)
        increment= cv2.getTrackbarPos('Increment', CoordinateTrackBar)
        if angle==maxangle or angle>maxangle:
            disX = math.cos(angle  * 3.14 / 180)
            disY = -math.sin(angle * 3.14 / 180)
            angle=minangle

        else:
            disX = math.cos(angle * 3.14 / 180)
            disY = -math.sin(angle * 3.14 / 180)
            angle += increment



        # disX = (LeftRight - 100)  # Displacement X from Center
        # disY = (UpDown - 100)  # Displacement Y From Center
        magXY = math.sqrt(disX * disX + disY * disY)  # (X,Y) Vector Magnitude
        if magXY==0:
            unitX.value = 0
            unitY.value = 0
        else:
            uX = (disX / magXY)  # X component of Unit Vector
            uY = (disY / magXY)  # Y component of Unit Vector
            unitX.value = uX
            unitY.value = uY

            cv2.arrowedLine(frame, (int(width / 2), int(height * 4 / 5)),
                        ((int(width / 2 + 50 * uX)), int(height * 4 / 5 + 50 * uY)), (255, 255, 0), 4)  # Unit Vector

        # unitX.value=-1
        # unitY.value=-0
        extraD.value =cv2.getTrackbarPos('ExtraData', CoordinateTrackBar)
        cv2.imshow('frame', frame)
        cv2.waitKey(1)