import cv2
import numpy as np

# Set Thresholds
lower_h = 154
lower_s = 114
lower_v = 150
upper_h = 255
upper_s = 255
upper_v = 255


# Create Trackbar
def emptyFunc():
    pass
HSVTrackBar = "HSV_TrackBars"
cv2.namedWindow(HSVTrackBar)
cv2.createTrackbar('L_H', HSVTrackBar, lower_h, 255, emptyFunc)
cv2.createTrackbar('L_S', HSVTrackBar, lower_s, 255, emptyFunc)
cv2.createTrackbar('L_V', HSVTrackBar, lower_v, 255, emptyFunc)
cv2.createTrackbar('U_H', HSVTrackBar, upper_h, 255, emptyFunc)
cv2.createTrackbar('U_S', HSVTrackBar, upper_s, 255, emptyFunc)
cv2.createTrackbar('U_V', HSVTrackBar, upper_v, 255, emptyFunc)

# Get Video Footage
capture = cv2.VideoCapture(0)
while (capture.isOpened()):
    reading_still, frame = capture.read()
    if reading_still:
        # Detect Red
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) # Convert TO HSV
        lower_h=cv2.getTrackbarPos('L_H',HSVTrackBar) # Get TrackBar Values
        lower_s = cv2.getTrackbarPos('L_S', HSVTrackBar)
        lower_v = cv2.getTrackbarPos('L_V', HSVTrackBar)
        upper_h=cv2.getTrackbarPos('U_H',HSVTrackBar)
        upper_s = cv2.getTrackbarPos('U_S', HSVTrackBar)
        upper_v = cv2.getTrackbarPos('U_V', HSVTrackBar)
        frame_mask = cv2.inRange(frame_hsv, np.array([lower_h, lower_s, lower_v]), np.array([upper_h, upper_s, upper_v])) # Create Mask within HSV Range
        frame_track=cv2.bitwise_and(frame,frame,mask=frame_mask) # Apply Mask on Frame
        # Display Results
        cv2.imshow('frame', frame)
        cv2.imshow('mask', frame_mask)
        cv2.imshow('red', frame_track)
        cv2.waitKey(1)
    else:
        capture.release()
        cv2.destroyAllWindows()
        break
