import cv2,matplotlib.pyplot
import yolo, time
from darkflow.net.build import TFNet
opt={'threshold': .4,'model': 'cfg/yolo.cfg', 'load': 'bin/yolo.weights','gpu':.7}
capture=cv2.VideoCapture(0)
predictor=TFNet(opt)
while (capture.isOpened()):
    stime=time.time();
    reading_still,frame=capture.read()
    if reading_still:
        objectList = predictor.return_predict(frame)
        for foundItem in objectList:
            box_corner = [(foundItem['topleft']['x'], foundItem['topleft']['y']),
                      (foundItem['bottomright']['x'], foundItem['bottomright']['y'])]
            itemLabel = foundItem['label']
            if (itemLabel != 'ALWAYSTRUENOITEM'):
                frame = cv2.rectangle(frame, box_corner[0], box_corner[1], (0, 0, 0), 5)
                frame= cv2.putText(frame,itemLabel,(foundItem['topleft']['x'], foundItem['topleft']['y']+100),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,0),2)
                print("Frame Rate:"+str(1/(time.time()-stime)))
        cv2.imshow('frame', frame)
        cv2.waitKey(1)
    else:
        capture.release()
        cv2.destroyAllWindows()
        break;