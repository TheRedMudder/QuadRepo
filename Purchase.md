# Phantom 4 Pro
## New

| Store | Option | Price | Notes |
| --- |  --- | --- | --- |
| [DJI Official](https://store.dji.com/product/phantom-4-pro) | (Standard RC) | $1499 | |
| [Amazon](https://www.amazon.com/DJI-CP-PT-000314-Phantom-4-Quadcopter/dp/B01MXLLDQ8?th=1) | Phantom 4 PRO | $1499 | Two Day Shipping |
| [Best Buy](https://www.bestbuy.com/site/dji-phantom-4-pro-quadcopter-white/5710367.p?skuId=5710367) | PRO | $1499 | Stock(Levittown, Westburry) |
| [Brooklyn Drones](https://www.brooklyndrones.nyc/hobbyist/dji-phantom-4-pro) |  | $1499 | |
| [B & H](https://www.bhphotovideo.com/c/product/1298124-REG/dji_cp_pt_000488_phantom_4_pro_quadcopter.html?ap=y&c3api=1876%2C92051678642%2C%2C&gclid=EAIaIQobChMIob_D7LW32gIVTVuGCh1D_An8EAkYASABEgJrzfD_BwE) | Remote with Mobile Device Holder | $1499 | |
## Refurbished
| Store | Option | Price | Notes |
| --- |  --- | --- | --- |
| [Drone Nerds](https://www.dronenerds.com/products/specials/openboxrefurbished/phantom-4-refurbished/dji-phantom-4-pro-quadcopter-4k-video-20mp-stills-dji-refurbished-model-p4prorefurb-dji.html?gclid=EAIaIQobChMImL2Kzc232gIVmI7ICh2lJA1sEAkYAiABEgKQd_D_BwE) | | $1279.00 | |
| [Amazon](https://www.amazon.com/DJI-CP-PT-000314-Phantom-4-Quadcopter/dp/B01MXLLDQ8?th=1) | Phantom 4 PRO | $1499 | Two Day Shipping |
| [Best Buy](https://www.bestbuy.com/site/dji-phantom-4-pro-quadcopter-white/5710367.p?skuId=5710367) | PRO | $1499 | Stock(Levittown, Westburry) |
| [Brooklyn Drones](https://www.brooklyndrones.nyc/hobbyist/dji-phantom-4-pro) |  | $1499 | |
| [B & H](https://www.bhphotovideo.com/c/product/1298124-REG/dji_cp_pt_000488_phantom_4_pro_quadcopter.html?ap=y&c3api=1876%2C92051678642%2C%2C&gclid=EAIaIQobChMIob_D7LW32gIVTVuGCh1D_An8EAkYASABEgJrzfD_BwE) | Remote with Mobile Device Holder | $1499 | |

# Accessories

- Phantom 4 Series Intelligent Flight Battery
- Phantom 4 Series Battery Charging Hub
