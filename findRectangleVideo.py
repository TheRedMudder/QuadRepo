import cv2
import numpy as np

# Get Video Footage
capture = cv2.VideoCapture(0)
while (capture.isOpened()):
    reading_still, frame = capture.read()
    if reading_still:
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame_blur = cv2.GaussianBlur(frame_gray, (5, 5), 0)  # Blur
        frame_tresh = cv2.threshold(frame_blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        # Detect Contour
        _, contours, _ = cv2.findContours(frame_tresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(frame,contours,-1,(255,0,0),3) # Draw All Contours in Blue
        # Detect Rect
        for contour in contours:
            contour_moments = cv2.moments(contour);  # Find Properties
            contour_X = int((contour_moments["m10"] / (contour_moments["m00"]+ + 1e-7)))
            contour_Y = int((contour_moments["m01"] / (contour_moments["m00"]+ + 1e-7)))
            # Find Rectangle
            verticies = len(cv2.approxPolyDP(contour, 0.04 * cv2.arcLength(contour, True), True))
            if verticies == 4:
                cv2.putText(frame, 'Rect', (contour_X, contour_Y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
                cv2.drawContours(frame, [contour], -1, (0, 0, 255), 3)

        # Display Results
        cv2.imshow('frame_g', frame_gray)
        cv2.imshow('frame', frame_tresh)
        cv2.imshow('edge', frame)
        cv2.waitKey(1)
    else:
        capture.release()
        cv2.destroyAllWindows()
        break
