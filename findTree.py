import cv2,matplotlib.pyplot
import yolo
from darkflow.net.build import TFNet
opt={'threshold': .4,'model': 'cfg/yolo.cfg', 'load': 'bin/yolo.weights','gpu':.7}
car_img='sample_img/sample_car.jpg'
predictor=TFNet(opt)
frame=cv2.cvtColor(cv2.imread(car_img, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
objectList=predictor.return_predict(frame)
for foundItem in objectList:
    box_corner=[(foundItem['topleft']['x'], foundItem['topleft']['y']),(foundItem['bottomright']['x'], foundItem['bottomright']['y'])]
    itemLabel=foundItem['label']
    if (itemLabel=='tree'):
        frame=cv2.rectangle(frame,box_corner[0],box_corner[1],(0,0,0),5)
matplotlib.pyplot.imshow(frame)
matplotlib.pyplot.show()
