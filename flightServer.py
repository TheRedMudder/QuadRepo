import math
import cv2
import numpy as np
import socket
import multiprocessing
import time

# Set Thresholds
lower_h = 154 # Lower HSV Values for Filtering RED
lower_s = 114
lower_v = 150
upper_h = 255 # Upper HSV Values for Filtering RED
upper_s = 255
upper_v = 255
MinRecSize=150 # Min Rectangle Size for Detection
MinCenterRadius=120 # Circle center's radius for rectangle classified as in the center of frame
UDP_Port=5842 # Computer Port Number(To Send coordinates From)
AFD_AddrPort=('192.168.1.3',5843) # Phones IP, Phone Port (To Send To coordinates to )

# Send Unit (X,Y) Method
def CoordinatesSenderSocket(unitX,unitY,extraD):
    s =socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # Initialize Socket
    ip_local=socket.gethostbyname(socket.gethostname()) # Get IP
    print("This systems local IP is: "+ip_local)
    ip_local="192.168.1.2"
    s.bind((ip_local,UDP_Port)) # Bind to phone
    while True:
        sendCooridnates="("+str(unitX.value)+", "+str(unitY.value)+", "+str(extraD.value)+")" # Format Coordinates as (x,y) Ex: (.2331,-.123)
        data=sendCooridnates.encode('utf-8')# Encode before sending
        s.sendto(data,AFD_AddrPort) # Send
        print(sendCooridnates)
        time.sleep(.3)

# Check if Main Process
if __name__=='__main__':
    # Before anything, we start a Socket To Send Data in a different Process
    unitX=multiprocessing.Value('d',0) # Create shared memory double
    unitY=multiprocessing.Value('d',0) # Create shared memory double
    extraD=multiprocessing.Value('d',0) # Create shared memory double
    multiprocessing.Process(target=CoordinatesSenderSocket, args=(unitX,unitY,extraD)).start() #Start CoordinatesSenderSocket on a separate process

    # Create Trackbar
    def emptyFunc(dontcare):
        pass
    HSVTrackBar = "HSV_TrackBars3"
    cv2.namedWindow(HSVTrackBar)
    cv2.createTrackbar('MinDetVal', HSVTrackBar, 55, 200, emptyFunc)
    cv2.createTrackbar('MaxDetVal', HSVTrackBar, 145, 200, emptyFunc)
    cv2.createTrackbar('L_H', HSVTrackBar, lower_h, 255, emptyFunc)
    cv2.createTrackbar('L_S', HSVTrackBar, lower_s, 255, emptyFunc)
    cv2.createTrackbar('L_V', HSVTrackBar, lower_v, 255, emptyFunc)
    cv2.createTrackbar('U_H', HSVTrackBar, upper_h, 255, emptyFunc)
    cv2.createTrackbar('U_S', HSVTrackBar, upper_s, 255, emptyFunc)
    cv2.createTrackbar('U_V', HSVTrackBar, upper_v, 255, emptyFunc)
    cv2.createTrackbar('MinSize', HSVTrackBar, MinRecSize, 5000, emptyFunc)
    cv2.createTrackbar('MinCenterRadius', HSVTrackBar, MinCenterRadius, 200, emptyFunc)





    # Get Video Footage
    capture = cv2.VideoCapture(0)
    while (capture.isOpened()):
        reading_still, frame = capture.read()
        if reading_still:
            in_center=False
            in_frontof=False
            height, width, _ = frame.shape
            # Detect Red
            frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) # Convert TO HSV
            lower_h=cv2.getTrackbarPos('L_H',HSVTrackBar) # Get TrackBar Values
            lower_s = cv2.getTrackbarPos('L_S', HSVTrackBar)
            lower_v = cv2.getTrackbarPos('L_V', HSVTrackBar)
            upper_h=cv2.getTrackbarPos('U_H',HSVTrackBar)
            upper_s = cv2.getTrackbarPos('U_S', HSVTrackBar)
            upper_v = cv2.getTrackbarPos('U_V', HSVTrackBar)
            MinRecSize = cv2.getTrackbarPos('MinSize', HSVTrackBar)
            MinCenterRadius = cv2.getTrackbarPos('MinCenterRadius', HSVTrackBar)
            frame_mask = cv2.inRange(frame_hsv, np.array([lower_h, lower_s, lower_v]), np.array([upper_h, upper_s, upper_v])) # Create Mask within HSV Range
            frame_track=cv2.bitwise_and(frame,frame,mask=frame_mask) # Apply Mask on Frame

            # Detect Edges
            frame_gray = cv2.cvtColor(frame_track, cv2.COLOR_BGR2GRAY)
            edges = cv2.Canny(frame_gray, cv2.getTrackbarPos('MinDetVal', HSVTrackBar), cv2.getTrackbarPos('MaxDetVal', HSVTrackBar))
            _, contours, _ = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            for i in range(0, len(contours)):
                approx = cv2.approxPolyDP(contours[i], cv2.arcLength(contours[i], True) * 0.02, True) # contour approximation with respect to perimeter
                if not (cv2.isContourConvex(approx)): #Skip Over
                    continue
                if (abs(cv2.contourArea(contours[i])) > MinRecSize): # Check if size is big enough
                    for current_pt in range(0, len(approx) - 1):
                        for connection_pt in range(current_pt + 1, len(approx)):
                            cv2.line(frame, (approx[current_pt][0][0],approx[current_pt][0][1]), (approx[connection_pt][0][0], approx[connection_pt][0][1]), (255, 255, 0), 1)
                        cv2.circle(frame, (approx[current_pt][0][0], approx[current_pt][0][1]), 3, (0, 255, 255), -1)
                    x,y,w,h = cv2.boundingRect(contours[i])
                    cv2.putText(frame, str(abs(cv2.contourArea(contours[i]))), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)  # Approximate Area in Pixels
                    cv2.drawContours(frame, [contours[i]], -1, (0, 0, 255), 3)
                    moment=cv2.moments(contours[i])
                    contourX = int(moment['m10'] / moment['m00'])
                    contourY = int(moment['m01'] / moment['m00'])
                    cv2.circle(frame, (contourX, contourY), 3, (0, 0, 255), -1)
                    goDirection=""# User Readable Direction for Drone To Go
                    try:
                        disX=(contourX-int(width/2)) # Displacement X from Center
                        disY=(contourY-int(height/2)) # Displacement Y From Center
                        magXY=math.sqrt(disX*disX+disY*disY) # (X,Y) Vector Magnitude
                        uX=(disX/magXY) # X component of Unit Vector
                        uY=(disY/magXY) # Y component of Unit Vector
                        unitX.value=uX
                        unitY.value=uY
                        cv2.arrowedLine(frame, (int(width / 2), int(height *4/ 5)),
                                 ((int(width / 2+50*uX)), int(height *4/ 5+50*uY)), (255, 255, 0), 4) # Unit Vector
                    except:
                        goDirection+="E"; # Show Error
                    in_frontof=False
                    in_center=True
                    if (int(width/2)-MinCenterRadius<contourX<int(width/2)+MinCenterRadius):
                        goDirection+="Cent-Hor, " # Move Forward
                    elif  (contourX<int(width/2)-MinCenterRadius):
                        goDirection+="Left, " # MOVE Left
                        in_center=False
                    elif (contourX > int(width / 2) + MinCenterRadius):
                        goDirection+="Right, "# MOVE Right
                        in_center=False
                    if (int(height/2)-MinCenterRadius<contourY<int(height/2)+MinCenterRadius):
                        goDirection+="Cent-Ver"# Move Forward
                    elif  (contourY<int(height/2)-MinCenterRadius):
                        goDirection+="Up"# MOVE Up
                        in_center=False
                    elif (contourY > int(height / 2) + MinCenterRadius):
                        goDirection+="Down"# MOVE Down
                        in_center=False
                    if (abs(cv2.contourArea(contours[i]))>60000):
                        goDirection="Orbiting"
                        in_frontof=True
                    if (not in_frontof) and in_center:
                        goDirection="Forward, On Path"
                    cv2.putText(frame, goDirection, (0, 50), cv2.FONT_HERSHEY_SIMPLEX, 1,
                                (255, 255, 255), 2, cv2.LINE_AA)
                    for pt in range(0, len(approx)):
                        cv2.circle(frame, (approx[pt][0][0],approx[pt][0][1]), 3, (0, 255, 255), -1)
                        cv2.line(frame, (approx[pt][0][0], approx[pt][0][1]),
                                 (int(width / 2), int(height / 2)), (255, 255, 255), 4)
            if in_center: #Show Circle if center of rectangle is within it
                circleadded=frame.copy()
                cv2.circle(circleadded, (int(width/2),int(height/2)), MinCenterRadius, (0, 255, 255), -1)
                frame=cv2.addWeighted(frame, 1, circleadded,.1,0)
                cv2.circle(frame, (int(width/2),int(height/2)), MinCenterRadius, (0, 255, 255), 5)

            if in_frontof:
                extraD.value=2
            elif in_center:
                extraD.value=1
            else:
                extraD.value=0
            # cv2.putText(frame, goDirection, (0, 50), cv2.FONT_HERSHEY_SIMPLEX, 1,
            #             (255, 255, 255), 2, cv2.LINE_AA)
            # cv2.addWeighted(background, 0.4, overlay, 0.1, 0)
            opp_edges=cv2.bitwise_not(edges)
            # Display Results
            cv2.imshow('frame_g', frame_gray)
            cv2.imshow('frame', frame)
            cv2.imshow('edge', edges)
            cv2.imshow('opp_edges', opp_edges)
            cv2.imshow('mask', frame_mask)

            cv2.imshow('track', frame_track)
            cv2.waitKey(1)
        else:
            capture.release()
            cv2.destroyAllWindows()
            break
else:
    print("Starting Socket in Separate Process")
