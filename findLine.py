import cv2
import numpy as np
# Get Image
img_loc = 'source/yolo/sample_img/sample_dog.jpg'  # Image Location
img = cv2.imread(img_loc, cv2.IMREAD_COLOR)
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# Detect Lines
edges = cv2.Canny(img_gray, 150, 250)
lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, maxLineGap=250, minLineLength=50)
# Draw Lines
for line in lines:
    x1, y1, x2, y2 = line[0]
    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 3)
# Display Results
cv2.imshow("Lines", img)
cv2.imshow("Gray", img_gray)
cv2.imshow("Edge", edges)
cv2.waitKey(0)
cv2.destroyAllWindows()
