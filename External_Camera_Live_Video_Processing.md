# Video of Live External Camera Processing
![Step 0](https://i.imgur.com/ClRhHWz.png)

Frame rates of camera were only 30 FPS. YOLOv2 was reading some frames multiple times, which is why the log is greater than 30 FPS. The screen capture software was only recording at 15 FPS.