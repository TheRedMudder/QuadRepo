# Benchmark Test
AVX2             |  NoAVX2
:-------------------------:|:-------------------------:
![AVX](https://imgur.com/5HhSsCb.png)  |  ![No AVX](https://imgur.com/w1J5eyU.png)
GPU 100 No AVX2             |  GPU 80% No AVX2
![AVX](https://imgur.com/tNvk3xo.png)  |  ![No AVX](https://imgur.com/K3Sa8yq.png)

GPU AVX2

![AVX](https://imgur.com/HdJbh3Z.png)
# Results 
Suprisingly, using the GPU with no AVX2 for the CPU out performed the GPU using AVX2 in speed. The result is unexpected because in theory, AVX2 will give performance gains for operations that only have CPU implmentations. Allocating a 100% of the GPUs memory to the process causes a CUDA out of memory error, but the system fallbacks to using the free GPU memory and marginally outperforms allocating 80% of the GPUs memory to the process. 
