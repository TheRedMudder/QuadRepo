
<img align="right" src="https://i.imgur.com/p8jenZt.png" width=15%>

# QuadRepo
[![Segal](https://img.shields.io/badge/Segal%20Structures%20Group-QuadRepo-blue.svg?style=flat-square)](http://edwardmsegal.com/)
<details>
 <summary>Table of Content (Click to Expand)
   
   
   </summary>

* [Getting Started](#getting-started) 
   * [![DownloadUpdateDroneCode](https://img.shields.io/badge/%20-Download%20UpdateDroneCode-blue.svg?style=flat-square&logo=github)](https://www.dropbox.com/s/f5itiplnt6ttaaa/UpdateDroneCode.bat?dl=1) or [![DownloadRepo](https://img.shields.io/badge/%20-Download%20Repo-blue.svg?style=flat-square&logo=github)](https://gitlab.com/TheRedMudder/QuadRepo/repository/master/archive.zip)
* [Launching Flight Server](#detecting-red-rectangles)
* [View Cars Only](#view-cars-only)
* [The Basic Loop](#the-basic-loop)
* [Depreciated](#depreciated)

</details>

## Getting Started

* Prerequisites: [Python 3.5+](https://www.python.org/downloads/), [TensorFlow](https://www.tensorflow.org/), [OpenCV](https://opencv.org/), and [Visual C++ 2015 Build Tools](http://landinghub.visualstudio.com/visual-cpp-build-tools)
  * [Windows Installation Guide](Installation.md)
* Download/Clone the repo
* Open CMD/Terminal in `QuadRepo\source` and run `build.bat`
  * YOLOv2 will start processing a test video.  
<p align="center"><img src="https://i.imgur.com/jLVT82B.png" width=95%></p>

## Launching Flight Server
Connect the Android and Computer to same WiFi network. Use AirDroid to relay the Androids screen to the computer. Use ManyCam to turn that video into a virtual webcam. Run the flightServer script.  
```python
python flightServer.py
```
<p align="center"><img src="https://i.imgur.com/IJ8OWhd.png" width=95%></p>

## View cars only
Place a sample image with at least one car in source/yolo/sample_img/ and name it `sample_car.jpg.` Next run the findCar script.
```python
python findCar.py
```
<p align="center"><img src="https://i.imgur.com/4LOKAsB.png" width=95%></p>

  <img src="https://i.imgur.com/BYDY8Vo.png" width=35% align="right">

## The Basic Loop
Live H.264 video data is decoded and sent to OpenCV where each frame is passed to YOLOv2 for object detection. The closest car is identified by it's x and y coordinates on the image, which is used to determine where the drone should move. The info is sent to DJI's API and the loop repeats. 

  <img src="https://i.imgur.com/igkSAoS.png" width=55% >
  
  DroneDeploy
  https://www.dronedeploy.com/
  https://www.ricoh.com/technology/institute/research/tech_flight_by_3d_vision.html
  https://www.youtube.com/watch?v=YWzYICu3PV0
  Stereo Vision, Monocular Vision, Ultrasonic, Infrared, Time-of-Flight and Lidar
  
# Drone Sensors
<img src="https://i.imgur.com/8KJjKdX.png" width=100% >

* accelerometers, gyroscopes, compasses, barometers, ultrasonic sensors, cameras and satellite positioning systems
# Precaution

*An aircraft can have a mass of several kilograms and move at speeds of up to 20 m/s. While the ability to programmatically change position is tremendously powerful, it also means an application can potentially damage the product it is controlling, or the environment the product is being controlled in.*

# [Hierarchy](https://developer.dji.com/mobile-sdk/documentation/introduction/sdk_architectural_overview.html)
<img src="https://devusa.djicdn.com/images/sdk-architectural-overview/Architecture-47cd8a771d.png" width=100% >
<img src="https://devusa.djicdn.com/images/sdk-architectural-overview/SDKAircraftArchitecture-ffe48f7d7d.png" width=100% >
Source: https://developer.dji.com/mobile-sdk/documentation/introduction/sdk_architectural_overview.html


# Basic Structure
The DJI SDK connects the Phantom 4 and 3rd party applications. A mobile(IOS/Android) or virtual device is connected to the remote controller with a USB cable. The remote controller acts as a gateway for exchanging data between the drone and a human input or program. The Python Object Detection scripts take in a encoded video file and output 2D Coordinates(x,y) that give the center of a box that encloses a selected tree that is in the frame. This is translated to a 3D movement using TapFly. 
![Basic Structure](https://imgur.com/xyhmyyC.png)

![Drone It App](https://imgur.com/lvxDUbg.png)


# Depreciated 
