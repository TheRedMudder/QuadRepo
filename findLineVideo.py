import cv2
import numpy as np

# Get Video Footage
capture = cv2.VideoCapture(0)
while (capture.isOpened()):
    reading_still, frame = capture.read()
    if reading_still:
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Detect Lines
        edges = cv2.Canny(frame_gray, 55, 145)
        lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 30, maxLineGap=10, minLineLength=50)
        # Draw Lines
        for line in lines:
            x1, y1, x2, y2 = line[0]
            cv2.line(frame, (x1, y1), (x2, y2), (0, 255, 0), 3)
        # Display Results
        cv2.imshow('frame_g', frame_gray)
        cv2.imshow('frame', frame)
        cv2.imshow('edge', edges)
        cv2.waitKey(1)
    else:
        capture.release()
        cv2.destroyAllWindows()
        break
