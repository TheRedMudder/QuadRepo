package com.ronjdias.autoflydrone;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

public class DrawingTheUnitVector extends View {
    float unitX=0;
    float unitY=0;
    public DrawingTheUnitVector(Context context) {
        super(context);
    }
    public void setXY(float X, float Y){
        unitX=X;
        unitY=Y;
        Log.d("SetXY",Float.toString(unitX)+","+Float.toString(unitY));

    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Rect ourRect=new Rect();

        ourRect.set(0,0,canvas.getWidth(),canvas.getHeight()/2);
        Paint blue = new Paint();
        blue.setColor(Color.BLUE);
        blue.setStyle(Paint.Style.FILL);
//        canvas.drawRect(ourRect,blue);
        Log.d("DrawingXY",Float.toString(unitX)+","+Float.toString(unitY));
        canvas.drawLine(canvas.getWidth()/2,canvas.getHeight()/2,canvas.getWidth()/2+100*unitX,canvas.getHeight()/2+100*unitY,blue);

        invalidate();
    }
}
