package com.ronjdias.autoflydrone;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.common.flightcontroller.virtualstick.FlightControlData;
import dji.common.flightcontroller.virtualstick.FlightCoordinateSystem;
import dji.common.flightcontroller.virtualstick.RollPitchControlMode;
import dji.common.flightcontroller.virtualstick.VerticalControlMode;
import dji.common.flightcontroller.virtualstick.YawControlMode;
import dji.common.mission.hotpoint.HotpointHeading;
import dji.common.mission.hotpoint.HotpointMission;
import dji.common.mission.hotpoint.HotpointStartPoint;
import dji.common.model.LocationCoordinate2D;
import dji.common.product.Model;
import dji.common.util.CommonCallbacks;
import dji.internal.a;
import dji.sdk.base.BaseComponent;
import dji.sdk.base.BaseProduct;
import dji.sdk.camera.VideoFeeder;
import dji.sdk.codec.DJICodecManager;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.timeline.TimelineElement;
import dji.sdk.mission.timeline.TimelineEvent;
import dji.sdk.mission.timeline.TimelineMission;
import dji.sdk.mission.timeline.actions.GoToAction;
import dji.sdk.mission.timeline.actions.HotpointAction;
import dji.sdk.mission.timeline.actions.LandAction;
import dji.sdk.mission.timeline.actions.TakeOffAction;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKManager;

public class MainActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener {
    /* DJI Variables */
    private static final String TAG = MainActivity.class.getName();
    public static final String FLAG_CONNECTION_CHANGE = "dji_sdk_connection_change";
    private static BaseProduct mProduct;
    private Handler mHandler;
    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };
    private List<String> missingPermission = new ArrayList<>();
    private AtomicBoolean isRegistrationInProgress = new AtomicBoolean(false);
    private static final int REQUEST_PERMISSION_CODE = 12345;
    private FlightController mFlightController; /* AutoFlyDrone Flight Controller For Virtual Sticks */
    Toast mToast;
    TextureView mVideoSurface;//For Video Display
    protected VideoFeeder.VideoDataCallback mReceivedVideoDataCallBack = null;
    private DJICodecManager mCodecManager = null;
    UDP_Server Server;//For Receiving coordinates
    Float VelocityX=0.0f;
    Float VelocityY=0.0f;
    Float VelocityZ=0.0f;
    Float Rotation=0.0f;

    private LocationCoordinate2D OrbitLocation;
    private LocationCoordinate2D TakeOffLocation;
    private float OrbitAltitude;
    int extraD=0;
    int currentCenter=0;

    SendVirtualStickDataTask mSendVirtualStickDataTask;
    Timer mSendVirtualStickDataTimer;
    AutoFlyDroneAPI AFD_API;
    Boolean flyDrone=false;
    Boolean goRightAndStop=false;

    /* DJI Variables END */
    /*DELETE VARIABLES START*/

    private TimelineEvent preEvent;
    private TimelineElement preElement;
    private DJIError preError;
    /*DELETE VARIABLES END*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // When the compile and target version is higher than 22, please request the following permission at runtime to ensure the SDK works well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }
        setContentView(R.layout.activity_main);
        mHandler = new Handler(Looper.getMainLooper());
        /*AFD START */
        Button btn_check = (Button) findViewById(R.id.afd_btn_check);

        Button btn_start = (Button) findViewById(R.id.afd_btn_start);

        Button btn_take_off = (Button) findViewById(R.id.afd_btn_take_off);

        Button btn_orbit = (Button) findViewById(R.id.afd_btn_orbit);

        Button btn_locate = (Button) findViewById(R.id.afd_btn_locate);

        Button btn_home = (Button) findViewById(R.id.afd_btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AFD_ReturnHome();
            }
        });
        btn_locate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AFD_Locate();
            }
        });
        btn_take_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AFD_TakeOff();
            }
        });
        btn_orbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AFD_Orbit();
            }
        });

        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshUI();//Shows when Drone is connected or disconnected
                initUI();//Shows Drone Video Stream
                UDPListener();//Shows Coordinates
            }
        });
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flyDrone){
                    ((Button) view).setText("Start Flight");
                    AFD_StopTrack();
                }else{
                    ((Button) view).setText("Stop Flight");
//                    goRightAndStop=false;
                    AFD_StartTrack();
                }
            }
        });
        AFD_API=new AutoFlyDroneAPI();
        /*AFD END */
    }
    /* DJI CODE START */

    /**
     * Checks if there is any missing permissions, and
     * requests runtime permission if needed.
     */
    private void checkAndRequestPermissions() {
        // Check for permissions
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(this, eachPermission) != PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        // Request for missing permissions
        if (missingPermission.isEmpty()) {
            startSDKRegistration();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            showToast("Need to grant the permissions!");
            ActivityCompat.requestPermissions(this,
                    missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE);
        }
    }

    /**
     * Result of runtime permission request
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Check for granted permission and remove from missing list
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int i = grantResults.length - 1; i >= 0; i--) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    missingPermission.remove(permissions[i]);
                }
            }
        }
        // If there is enough permission, we will start the registration
        if (missingPermission.isEmpty()) {
            startSDKRegistration();
        } else {
            showToast("Missing permissions!!!");
        }
    }

    private void startSDKRegistration() {
        if (isRegistrationInProgress.compareAndSet(false, true)) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    showToast("registering, pls wait...");

                    DJISDKManager.getInstance().registerApp(MainActivity.this.getApplicationContext(), new DJISDKManager.SDKManagerCallback() {
                        @Override
                        public void onRegister(DJIError djiError) {
                            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {
                                showToast("Register Success");
                                DJISDKManager.getInstance().startConnectionToProduct();
                            } else {
                                showToast("Register sdk fails, please check the bundle id and network connection!");
                            }
                            Log.v(TAG, djiError.getDescription());
                        }

                        @Override
                        public void onProductDisconnect() {
                            mProduct = null;
                            Log.d(TAG, "onProductDisconnect");
                            refreshUI();
                            notifyStatusChange();
                        }

                        @Override
                        public void onProductConnect(BaseProduct baseProduct) {
                            mProduct = baseProduct;
                            Log.d(TAG, "onProductConnect");
                            refreshUI();
                            notifyStatusChange();
                        }

                        @Override
                        public void onComponentChange(BaseProduct.ComponentKey componentKey,
                                                      BaseComponent oldComponent,
                                                      BaseComponent newComponent) {
                            if (newComponent != null) {
                                newComponent.setComponentListener(mDJIComponentListener);
                            }
                            Log.d(TAG,
                                    String.format("onComponentChange key:%s, oldComponent:%s, newComponent:%s",
                                            componentKey,
                                            oldComponent,
                                            newComponent));

                            notifyStatusChange();
                            refreshUI();
                        }

                    });
                }
            });
        }

    }

    private BaseComponent.ComponentListener mDJIComponentListener = new BaseComponent.ComponentListener() {
        @Override
        public void onConnectivityChange(boolean isConnected) {
            refreshUI();/* AutoFlyDrone Update UI */
            notifyStatusChange();
        }
    };

    private void notifyStatusChange() {
        mHandler.removeCallbacks(updateRunnable);
        mHandler.postDelayed(updateRunnable, 500);
    }

    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(FLAG_CONNECTION_CHANGE);
            sendBroadcast(intent);
        }
    };

    private void showToast(final String toastMsg) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mToast == null) {
                    mToast = Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT);
                }
                if (!mToast.getView().isShown()) {
                    mToast.setText(toastMsg);
                    mToast.show();
                }
            }
        });
    }

    /* DJI CODE END */
    // Refreshes Connection Status
    private void refreshUI() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                TextView tv_ready = (TextView) findViewById(R.id.afd_tv_ready);
                if (AFD_FlightControllerAvaliable()) {
                    tv_ready.setText("Ready");
                } else {
                    tv_ready.setText("Not Connected");
                }
            }
        });
    }
    //Shows Coordinates Received from Python Script
    private void refreshCoordinates(final String coordinates) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                   TextView tv_coordinates = (TextView) findViewById(R.id.afd_tv_coordinates);
                   tv_coordinates.setText(coordinates);
            }
        });
    }
    private Boolean AFD_FlightControllerAvaliable() {
        //Checks if drone connected and flight controller is ready
        if (mProduct != null) {
            if (mProduct instanceof Aircraft) {
                Aircraft mAircraft = (Aircraft) mProduct;
                mFlightController = mAircraft.getFlightController();
                return true;
            }
        }
        return false;
    }
    private Boolean AFD_FlightControllerAvaliable(Boolean noErrorMSG) {
        //Conditionally show error message on error
        if (!noErrorMSG) {
            return AFD_FlightControllerAvaliable();// If noErrorMSG=false we just return AFD_FlightControllerAvaliable() value
        } else {
            // If noErrorMSG=True&FlightControllAvaliable=False: Display Error Message
            if (AFD_FlightControllerAvaliable()) {
                return true;
            } else {
                AFD_Error("No flight Controller");
                return false;
            }
        }

    }
    private void AFD_Error(String e_message) {
        showToast(e_message); // Display all errors
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (mCodecManager == null) {
            mCodecManager = new DJICodecManager(getApplicationContext(), surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    private void initUI() {
        // init mVideoSurface
        TextureView mVideoSurface = (TextureView) findViewById(R.id.video_previewer_surface);

        if (null != mVideoSurface) {
            mVideoSurface.setSurfaceTextureListener(this);
        }
        mReceivedVideoDataCallBack = new VideoFeeder.VideoDataCallback() {
            @Override
            public void onReceive(byte[] videoBuffer, int size) {
                if (mCodecManager != null) {
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                }
            }
        };
        if (mVideoSurface.isAvailable()) {
            onSurfaceTextureAvailable(mVideoSurface.getSurfaceTexture(), mVideoSurface.getWidth(), mVideoSurface.getHeight());
        }
        initPreviewer();
    }

    private void initPreviewer() {
        try {
            VideoFeeder.getInstance().getPrimaryVideoFeed().setCallback(mReceivedVideoDataCallBack);
            showToast("Displaying Video");

        } catch (Exception ignored) {
            showToast("Couldn't Display Video");

        }


    }

    private void UDPListener() {
        if (Server!=null){
            Server.stop_UDP_Server();
            Server=null;
        }else{
            Server = new UDP_Server();
            Server.startUDP();
        }

    }

    public String parseXY(String xy){
        String[] xyArray = xy.split("\\s*,\\s*");
        float x=Float.parseFloat(xyArray[0].substring(1));
        float y = Float.parseFloat(xyArray[1]);
        float extraData = Float.parseFloat(xyArray[2].substring(0,xyArray[2].length()-1));
//        showToast("X"+xyArray[0].substring(1)+"Y"+xyArray[1]+"e:"+xyArray[2].substring(0,xyArray[2].length()-1));
        VelocityY=x*.5f;//Max 10, but fly with .5 multiplier
        VelocityZ=-y*.5f;//Max 2, but fly with .5 multiplier
        extraD=(int)extraData;
        return xy;
    }
    public class UDP_Server
    {
        private AsyncTask<Void, Void, Void> async;
        private boolean Server_alive = true;
        public void startUDP()
        {
            refreshCoordinates("UDP Started");
            async = new AsyncTask<Void, Void, Void>()
            {
                @Override
                protected Void doInBackground(Void... params)
                {
                    byte[] message = new byte[50];
                    DatagramPacket p = new DatagramPacket(message, message.length);
                    DatagramSocket s = null;
                    try
                    {
                        s = new DatagramSocket(5843);
                        while(Server_alive)
                        {
                            s.receive(p);

                            refreshCoordinates(parseXY(new String(message, 0, p.getLength())));

                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        showToast(e.toString());
                    }
                    finally
                    {
                        if (s != null)
                        {
                            refreshCoordinates("Closing UDP");
                            s.close();
                        }
                        refreshCoordinates("UDP Closed");

                    }

                    return null;
                }
            };

            if (Build.VERSION.SDK_INT >= 11) async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else async.execute();
        }

        public void stop_UDP_Server()
        {
            Server_alive = false;
        }

    }
    private void AFD_StopTrack(){
        flyDrone=false;
    }
    private void AFD_TakeOff(){
        if (AFD_FlightControllerAvaliable()) {
            /* Start Code For Take Off*/

                TakeOffLocation = new LocationCoordinate2D(mFlightController.getState().getAircraftLocation().getLatitude(),mFlightController.getState().getAircraftLocation().getLongitude());



            mFlightController.startTakeoff(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError != null) {
                        AFD_Error(djiError.getDescription());
                    }
                }
            });
            /* End Code For Take Off*/
        }else{
            ((Button) findViewById(R.id.afd_btn_take_off)).setText("TakeOff Failed");
        }
    }
    private void AFD_ReturnHome(){
        if (AFD_FlightControllerAvaliable()) {
            /* Start Code For Take Off*/
            mFlightController.startGoHome(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    if (djiError != null) {
                        AFD_Error(djiError.getDescription());
                    }
                }
            });
            /* End Code For Take Off*/
        }else{
            ((Button) findViewById(R.id.afd_btn_home)).setText("Home Failed");
        }
    }
    private void AFD_StartTrack(){
        if (AFD_FlightControllerAvaliable()){

            /* Start Code For Red Rectangle*/
                flyDrone=true;
                if (mSendVirtualStickDataTimer==null){
                    mFlightController.setVirtualStickModeEnabled(true, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            showToast("Result: " + (djiError == null
                                    ? "Success"
                                    : djiError.getDescription()));
                            mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
                            mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
                            mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
                            mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);

                            mSendVirtualStickDataTask = new SendVirtualStickDataTask();
                            mSendVirtualStickDataTimer = new Timer();
                            mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 100, 200);

                            showToast("HI");
                        }
                    });
                }
            /* End Code For Red Rectangle*/

        }else{
            flyDrone=false;
            ((Button) findViewById(R.id.afd_btn_start)).setText("Flight Didn't Start");
        }

    }
    private void AFD_Locate(){
        if (AFD_FlightControllerAvaliable()){
            OrbitLocation = new LocationCoordinate2D(mFlightController.getState().getAircraftLocation().getLatitude(),mFlightController.getState().getAircraftLocation().getLongitude());
            float altitude=mFlightController.getState().getAircraftLocation().getAltitude();
            if (altitude< HotpointMission.MAX_ALTITUDE&&altitude> HotpointMission.MIN_ALTITUDE){
                OrbitAltitude=altitude;
            }else{
                OrbitAltitude=10.0f;
            }
            showToast("("+String.valueOf(mFlightController.getState().getAircraftLocation().getLatitude())+", "+String.valueOf(mFlightController.getState().getAircraftLocation().getLongitude())+")");

        }else{
            ((Button) findViewById(R.id.afd_btn_start)).setText("Locate Failed");
        }
        }
    private void AFD_Orbit(){
        if (AFD_FlightControllerAvaliable()){
            /* Start ORBIT*/
            List<TimelineElement> elements = new ArrayList<>();

            MissionControl missionControl = MissionControl.getInstance();
            final TimelineEvent preEvent = null;
            MissionControl.Listener listener = new MissionControl.Listener() {
                @Override
                public void onEvent(@Nullable TimelineElement element, TimelineEvent event, DJIError error) {
                    updateTimelineStatus(element, event, error);
                }
            };
            showToast("Starting 270 degree hotpoint");
            HotpointMission hotpointMission = new HotpointMission();
            if (OrbitLocation==null){
                double homeLatitude = 40.713723;
                double homeLongitude = -73.604862;
                OrbitLocation=new LocationCoordinate2D(homeLatitude, homeLongitude);
                OrbitAltitude=10;
            }
            hotpointMission.setHotpoint(OrbitLocation);
            hotpointMission.setAltitude((double) OrbitAltitude);
            hotpointMission.setRadius(7);
            hotpointMission.setAngularVelocity(10);
            HotpointStartPoint startPoint = HotpointStartPoint.NEAREST;
            hotpointMission.setStartPoint(startPoint);
            HotpointHeading heading = HotpointHeading.ALONG_CIRCLE_LOOKING_FORWARDS	;
            hotpointMission.setHeading(heading);
            hotpointMission.setClockwise(false);
            elements.add(new HotpointAction(hotpointMission, 250));
            LandAction autoLand = new LandAction();
            autoLand.setAutoConfirmLandingEnabled(true);
            elements.add(autoLand);
            if (TakeOffLocation!=null){
                elements.add(new GoToAction( TakeOffLocation, 0));
            }


            if (missionControl.scheduledCount() > 0) {
                missionControl.unscheduleEverything();
                missionControl.removeAllListeners();
            }

            missionControl.scheduleElements(elements);
            missionControl.addListener(listener);

            if (MissionControl.getInstance().scheduledCount() > 0) {
                MissionControl.getInstance().startTimeline();
            } else {
                showToast("Couldn't start Mission");
            }
            /* END ORBIT*/

        }
            }
    /*DJI TIMELINE REMOVE START */
    private void updateTimelineStatus(@Nullable TimelineElement element, TimelineEvent event, DJIError error) {

        if (element == preElement && event == preEvent && error == preError) {
            return;
        }

        if (element != null) {
            if (element instanceof TimelineMission) {
                showToast(((TimelineMission) element).getMissionObject().getClass().getSimpleName()
                        + " event is "
                        + event.toString()
                        + " "
                        + (error == null ? "" : error.getDescription()));
            } else {
                showToast(element.getClass().getSimpleName()
                        + " event is "
                        + event.toString()
                        + " "
                        + (error == null ? "" : error.getDescription()));
            }
        } else {
            showToast("Timeline Event is " + event.toString() + " " + (error == null
                    ? ""
                    : "Failed:"
                    + error.getDescription()));
        }

        preEvent = event;
        preElement = element;
        preError = error;
    }
    private void AFD_GoRightAndStop(){
        if (goRightAndStop){

            return;
        }
        goRightAndStop=true;
        AFD_Locate();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                AFD_StopTrack();
//                goRightAndStop=false;
                AFD_Orbit();
//                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        AFD_ReturnHome();
//                    }
//                }, 45000);
            }
        }, 2000);
    }
    /*DJI TIMELINE REMOVE END */

    class SendVirtualStickDataTask extends TimerTask{
        @Override
        public void run() {
            if (flyDrone){
                if (extraD==2){
//                    showToast("Ready to Arc");
                    AFD_GoRightAndStop();
                    mFlightController.sendVirtualStickFlightControlData(new FlightControlData( 2.5f,0.0f, 0.0f, 0), new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                Log.d("ANDROIDERROR_DJI_QUAD",djiError.getDescription());
                                showToast("Wupz"+djiError.getDescription());
                            }
                        }
                    });

                }
                if (extraD==0){
                    //(0(Rotate Left/Right-10),Z(Up/Down-10),0(Left-Right-3),0(Forward/Back-2)
                    //(0(Really  Left/Right-10),Z(Really Forward/Back-10),0(Rotate Left-Right-3),0(Really UP DOWN)
                    mFlightController.sendVirtualStickFlightControlData(new FlightControlData( (float)VelocityY, (float)VelocityX, (float)Rotation, (float)VelocityZ), new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                Log.d("ANDROIDERROR_DJI_QUAD",djiError.getDescription());
                                showToast("Wupz"+djiError.getDescription());
                            }
                        }
                    });
                }else if (extraD==1){
                    //(0(Rotate Left/Right-10),Z(Up/Down-10),0(Left-Right-3),0(Forward/Back-2)
                    //(0(Really  Left/Right-10),Z(Really Forward/Back-10),0(Rotate Left-Right-3),0(Really UP DOWN)
                    mFlightController.sendVirtualStickFlightControlData(new FlightControlData( 0.0f,1.0f, 0.0f, 0), new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            if (djiError != null) {
                                Log.d("ANDROIDERROR_DJI_QUAD",djiError.getDescription());
                                showToast("Wupz"+djiError.getDescription());
                            }
                        }
                    });
                }


            }

        }
    }
    private void orbitDJI(){

    }
}
