# To Do List
- Scan Folder for Image, Send Image - > Recieve Image
- Scan Folder for Multiple Images -> Recieve Multiple Images
- Send Images to Python OpenCV or animate images in folder for processing
- See speed and if you can detect edge. Optimize speed
- Fly in house
- White Balance Detect Object
- Test Controls from PC



















<p align="center" width=100%>
  <a href="#semi-autonomous-drones-deploying-bridges">General Overview</a> •
  <a href="https://vimeo.com/282719041">Demo</a> •
  <a href="https://gitlab.com/TheRedMudder/QuadRepo/repository/master/archive.zip">Download</a> •
  <a href="http://edwardmsegal.com/polyester-rope-suspended-footbridges/">Credits</a> •
  <a href="https://gitlab.com/TheRedMudder/QuadRepo/wikis/home">Documentation</a>
  
</p>
<img align="right" src="https://i.imgur.com/p8jenZt.png" width=15%>

# QuadRepo [![Segal](https://img.shields.io/badge/Segal%20Structures%20Group-QuadRepo-blue.svg?style=for-the-badge)](http://edwardmsegal.com/)

<details>
 <summary>Table of Content (Click to Expand)</summary>
<ul>
<li><a href="#getting-started">Getting Started</a></li>
<li><a href="#launching-flight-server">Launching Flight Server</a></li>
<li><a href="#variables-for-flight-server">Variables for Flight Server</a></li>
</ul>
</details>
 


## Semi-Autonomous Drones Deploying Bridges

 This project researches the ability to semi-autonomously wrap a rope around a particular object (ex: trees or red rectangular sign) for the deployment of a temporary bridge. The current focus is on the initial drone’s flight.
 <img src="https://imgur.com/ofkrca0.png" width=50% align="right"> 


* 1- Take off
* 2- Find and fly toward sign (Red Rectangle)
* 3- Fly to right of it
* 4- Arc 270 degrees
* 5- Fly back and land

<p align="center"><a href="https://vimeo.com/282719041"><img src="https://imgur.com/VXTlWOQ.png" width=95%></a></p>

## Getting Started
[![DownloadUpdateDroneCode](https://img.shields.io/badge/%20-Download%20UpdateDroneCode-blue.svg?style=flat-square&logo=github)](https://www.dropbox.com/s/f5itiplnt6ttaaa/UpdateDroneCode.bat?dl=1)  [![DownloadRepo](https://img.shields.io/badge/%20-Download%20Repo-blue.svg?style=flat-square&logo=github)](https://gitlab.com/TheRedMudder/QuadRepo/repository/master/archive.zip)

* Prerequisites: [Python 3.5+](https://www.python.org/downloads/), [ManyCam](https://manycam.com/), [AirDroid](https://www.airdroid.com/), and [OpenCV](https://opencv.org/)
* Download/Clone the repo + Install Android APK
* Setup ManyCam Virtual Webcam to Record Screen of AirDroid mirroring Android
* Open CMD/Terminal in `QuadRepo` and run `flightServer.py`
    

## Launching Flight Server
Connect the Android and Computer to same WiFi network. Use AirDroid to relay the Androids screen to the computer. Use ManyCam to turn that video into a virtual webcam. Run the flightServer script.  
```python
python flightServer.py
```

## Step by Step
1) Install the APK on your Android Phone and DJI GOV4
   - [Install Android Studios and Java JDK](https://www.youtube.com/watch?v=ABm5bgKIY1A) or your favorite IDE
      - [Import the project (AutoFlyDrone) into Android Stuidios](https://www.youtube.com/watch?v=u1406f2e88w) or other IDE
      - [Sync with Gradle Files](https://www.youtube.com/watch?v=MUbRRgvHh1w), [Hit Build](https://stackoverflow.com/questions/28385172/run-button-is-disabled-in-android-studio), Hit Run and the AutoFlyDrone will be installed
    - Download [DJI GO 4](https://play.google.com/store/apps/details?id=dji.go.v4&hl=en_US) from the Play Store
      - Make an [DJI Account](https://account.dji.com/login) and configure whatever settings you want about the drone
2) Add Exception to Firewall for PORT 5842 and 5843
3) 
    
Connect the Android and Computer to same WiFi network. Use AirDroid to relay the Androids screen to the computer. Use ManyCam to turn that video into a virtual webcam. Run the flightServer script.  
```python
python flightServer.py
```

## Variables for Flight Server
`lower_h`=`154`,`lower_s`=`114`,`lower_v`=`150`
* Lower HSV values for filtering color RED. Any H,S, or V lower than these lower bounds will be filtered out. These values can be adjusted for other colors or different lightings with the slider or hardcoding the adjustment. The min value is 0. 

`upper_h`=`255`,`upper_s`=`255`,`upper_v`=`255`
* Upper HSV values for filtering color RED. Any H,S, or V higher than these upper bounds will be filtered out. These values can be adjusted for other colors or different lightings with the slider or hardcoding the adjustment. The max value is 255.

`MinRecSize`=`150`
* The minimum rectangle size for detection. Any rectangles below this size will be ignored. This values can be adjusted for other colors or different lightings with the slider or hardcoding the adjustment. There is no max or min, but it doesn't make sense to be above 4/5rds of the total pixels in the frame or go below 150.

`MinCenterRadius`=`?120?`
* The current default is 120, but this will change in the future. This variable adjust the center circle radius. The center circle is used to determine if the rectangle is classified in the center or not. Making the radius to bigger may make the flight faster, but also may lead to the drone losing the sign. Making the radius small will result in slower speeds, but also makes it less likely for the drone to losing the sign. This values can be adjusted for other colors or different lightings with the slider or hardcoding the adjustment.

`UDP_Port`=`5842`
* The computer port number to bind the python script to. The default is 5842, which was picked randomly. This can be changed to any open port number. Do not use ports used by other applications or common ports. 

`AFD_AddrPort`=`5843`
* The Androids port number to send the data to. The default is 5843, which was picked randomly. This can be changed to any open port number as long as you also change the port in the Android app. Do not use ports used by other applications or common ports. 


[![Segal](https://img.shields.io/badge/Segal%20Structures%20Group-QuadRepo-blue.svg?style=flat-square)](http://edwardmsegal.com/)

---


<p align="center">
  <strong><a href="https://gitlab.com/TheRedMudder/QuadRepo/wikis/home">Read Our Docs</a></strong> - 
  <strong><a href="https://developer.dji.com/mobile-sdk/documentation/introduction/index.html">DJI's Docs</a></strong>
  
</p>
