# Step 0: Make sure Nvidia Drivers are up to date.
If all the drivers are installed correctly, skip directly to step 1.
- DDU(Completely Uninstall Old Driver):
  - http://www.guru3d.com/files-details/display-driver-uninstaller-download.html  
- NVidia Drivers(Click Custom Install and then check Perform a Clean Install): 
  - http://www.nvidia.com/Download/Find.aspx
    ![Step 0](https://imgur.com/joaLSaS.png)
    ![Step 1](https://imgur.com/20dv7bU.png)
    ![Step 2](https://imgur.com/PuBl4uK.png)

# Step 1: Install TensorFlow with GPU support!
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `No Longer Compiling ourself` ~~Compiling it ourselves allows us to optimize the build for our CPU by supporting AVX2. Although most of our code will be running on the GPU, the AVX2 will give us a slight improvement for the parts that only can run on the CPU. Since we are only testing we won't bother with compiling from source for now. We will then compile from source. These instuctions are at the end.~~ ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `Updates:` We don't need AVX2 because of results from our [Benchmark](Benchmark.md)
- Official Install Instructions for Windows/Install From Source:
  - https://www.tensorflow.org/install/install_windows
  - ~~https://www.tensorflow.org/install/install_sources~~
- Prerequisites: [Python 3.5.X](https://www.python.org/downloads/), [CUDA Toolkit 9.0](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/), [cuDNN v7.0.](https://developer.nvidia.com/cudnn)
## Step 1A: Installing TensorFlow GPU Dependencies
 - [Visual Studio 2017 With C++ Runtime](https://www.visualstudio.com/) - This is required for CUDA to function properly!
 - [Install CUDA](https://developer.nvidia.com/cuda-90-download-archive?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exelocal) - Standard install is fine, GeForce Experience is not required.
 - [Download cuDNN v7.1.4](https://developer.nvidia.com/rdp/cudnn-download)
 - Extract cuDNN and open cuda\bin and place `cudnn64_7.dll` in the CUDA install directory(`C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\bin`)
 
 ![Step 0](https://imgur.com/8g9mmVI.png)
 
 - Place "`cudnn.h`" in `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\include\`
 
 ![Step 0](https://imgur.com/N6YxGza.png)
 

 - Place "`cudnn.lib`" in `C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\lib\x64\`
 
 
 ![Step 0](https://imgur.com/1T5Qasc.png)
 ## Step 1B: Installing TensorFlow General Dependencies
  - Install [Python 3.6](https://www.python.org/downloads/release/python-360/) or [Anaconda 3.6](https://www.anaconda.com/download/) and make sure it is added to the PATH
  
 
 ![Step 0](https://imgur.com/wB1CgUh.png)
 
 
 ## Step 1C: Installing TensorFlow GPU (using official source not custom built)
 
 Type `pip install --upgrade tensorflow-gpu` in `CMD`
 
# Step 2: Downloading YOLO Dependencies and Configuring Code
Now we are ready to use [YOLO(Darkflow Repo)](https://github.com/thtrieu/darkflow), but we don't need to download it because it is included in QuadRepo. We will still have to install YOLOv2 dependencies.
 - [Install OpenCV](https://www.lfd.uci.edu/~gohlke/pythonlibs/) with PIP
 - [Download 608x608 weights](https://pjreddie.com/darknet/yolov2/)
 - Create a new "bin" folder in QuadRepo\source\yolo and place the weights in there. If the weight is called "yolov2.weights" rename it to "yolo.weights"
 - Run build.bat in QuadRepo.
 - If after running the file a video exist in QuadRepo\output then everything is working succesfully!
 
 ![Step 0](https://imgur.com/LZPrhsd.png)
 ![Step 0](https://imgur.com/Cru6AAE.png)
## ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) (Use tensorflow-windows-wheel Instead)! Advanced: Optimizing by Compiling From Source 
  - ~~Install Python 3.5 and make sure python and pip is added to path.~~
  - ~~Install [CMake](https://cmake.org/download/) 3.5 or later(3.11.3) and add it to the PATH~~
    ![Step 0](https://imgur.com/lXjgW6M.png)    
  - ~~Install [GIT](https://git-scm.com/)~~
  - ~~Install [Visual Studios 2015](https://www.visualstudio.com/vs/older-downloads/) (Visual Studios 2017 does not work!)~~
 ![Step 0](https://imgur.com/cpJGwq6.png)
 ![Step 1](https://imgur.com/QmFReTq.png)
## Tensorflow-windows-wheel Instead
- Download the [Tensorflow-windows-wheel](https://github.com/fo40225/tensorflow-windows-wheel) file with whichever version of CUDA and cuDNN you plan to use.
- Install with pip
# DEMO Testing
The following test were run on a Windows 10 system that has an Intel i5-8400 with 16 GB RAM and NVIDIA GeForce GTX 1080 Ti with 11 GB dedicated memory.
## GPU Support with AVX2
![Step 1](https://imgur.com/4m6jAUG.png)
## GPU Support without AVX2
![Step 1](https://imgur.com/DdA1yB8.png)
## GPU Support without AVX2 80% Allocation
The CUDA out of memory occurs when TensorFlow can't allocate the set fraction of the GPU memory to the process. The  fallback will still utilize the free GPU memory and not revert to CPU only computations. This error can be avoided by changing "--gpu 1.0" in "source\yolo\processVideo.bat" to a lower number.
![Step 1](https://imgur.com/21gyjqy.png)

## AVX2 
![Step 1](https://imgur.com/7s7YeYP.png)

## NO AVX2
![Step 1](https://imgur.com/VmvKJFE.png)
# Common Errors
![Step 1](https://imgur.com/JHsqtgr.png)
- YOLO is not built! Run "`python setup.py build_ext --inplace`" in yolo directory

![Step 1](https://imgur.com/J5OKh0v.png)
- NVIDIA's installer code is has a single try catch block that catches all exceptions without giving any error codes. So, there are many ways to get this installer failed message. Assuming that no anti-virus blockers is blocking the software, the installer is run as adminsistrator, there isn't a previous version installed, and no task is hogging the GPU; you can try the following steps will most likely solve the issue:
  - Extract the exe with 7-zip or any other file extractor. Renaming the zip to an exe to zip and using windows built in extractor sometimes work.
  - Run the setup.exe
  - Hit Custom (Advanced)
  - Uncheck Visual Studio Integration
  - Go to CUDAVisualStudioIntegration and run the two msi files(NVIDIA NVTX Installer.x86_64.Release.v1.21018621.Win64,NVIDIA_Nsight_Visual_Studio_Edition_Win64_5.4.0.17229)
  - Copy files from "`extras\visual_studio_integration\MSBuildExtensions`" into "`C:\Program Files (x86)\MSBuild\Microsoft\Portable\v4.0`" & "`C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\VC\VCTargets\BuildCustomizations`"
# Step 3 Install GenyMotion & Intellij IDEA
This last installation is user friendly, so an explanation is not necessary. Nevertheless, I linked YouTube videos with the steps.
- ~~[GenyMotion](https://www.youtube.com/watch?v=5MypWPkXaDw])~~ ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `Updates:` We can't use GenyMotion because of licensing. Instead we will use a real android with [AirDroid](https://www.airdroid.com/) to mirror the screen to a PC.
  - ~~[Download](https://www.genymotion.com/fun-zone/)~~
- [Intellij IDEA](https://www.jetbrains.com/idea/download/) or [Android Studios](https://developer.android.com/studio/)
  - [Additional Steps for Intellij IDEA](https://www.youtube.com/watch?v=2IqfF4WMKGc)
    - https://github.com/codepath/android_guides/wiki/Setting-up-IntelliJ-IDEA
